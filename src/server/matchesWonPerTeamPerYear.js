function matchesWonPerTeamPerYear(matchData) {
  let matchesWonByTeamPerYear = {};
  for (let index in matchData) {
    let winner = matchData[index].winner;
    let year = matchData[index].season;
    if (winner in matchesWonByTeamPerYear) {
      if (year in matchesWonByTeamPerYear[winner]) {
        matchesWonByTeamPerYear[winner][year] += 1;
      } else {
        matchesWonByTeamPerYear[winner][year] = 1;
      }
    } else {
      matchesWonByTeamPerYear[winner] = {};
      matchesWonByTeamPerYear[winner][year] = 1;
    }
  }

  for (let winner in matchesWonByTeamPerYear) {
    if (winner === "") {
      delete matchesWonByTeamPerYear[""];
    }
  }
  return matchesWonByTeamPerYear;
}

module.exports = matchesWonPerTeamPerYear;

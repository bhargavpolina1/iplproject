const csvMatchFilePath = "/home/phanipolina/Desktop/ipl/src/data/matches.csv";
const csvDeliveriesFilePath =
  "/home/phanipolina/Desktop/ipl/src/data/deliveries.csv";

const csv = require("csvtojson");
var fs = require("fs");

const matchesPerYear = require("./matchesPerYear.js");
const matchesWonPerTeamPerYear = require("./matchesWonPerTeamPerYear.js");
const extraRunsInYear = require("./extraRunsPerTeam.js");
const economicBowlers = require("./economicBowlers.js");
const tossesAndWins = require("./tossAndWins.js");
const maxPlayerOfTheMatchPerSeason = require("./maxPlayerOfTheMatchPerSeason.js");
const strikeRate = require("./strikeRate.js");
const playerDismissedByPlayer = require("./playerDismissedByPlayer.js");
const economicBowlerSuperOver = require("./economicBowlerSuperOver.js");

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    let result = matchesPerYear(matchData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/matchesPerYear.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    let result = matchesWonPerTeamPerYear(matchData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/matchesWonPerTeamPerYear.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    csv()
      .fromFile(csvDeliveriesFilePath)
      .then((deliveriesData) => {
        let result = extraRunsInYear(deliveriesData, matchData);
        fs.writeFile(
          "/home/phanipolina/Desktop/ipl/src/public/output/extraRunsPerTeam.json",
          JSON.stringify(result),
          function (err) {
            if (err) throw err;
            console.log("Results pushed sucessfully.");
          }
        );
      });
  });
csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    csv()
      .fromFile(csvDeliveriesFilePath)
      .then((deliveriesData) => {
        let result = economicBowlers(matchData, deliveriesData);
        fs.writeFile(
          "/home/phanipolina/Desktop/ipl/src/public/output/economicBowlers.json",
          JSON.stringify(result),
          function (err) {
            if (err) throw err;
            console.log("Results pushed sucessfully.");
          }
        );
      });
  });

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    let result = tossesAndWins(matchData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/tossAndWins.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    let result = maxPlayerOfTheMatchPerSeason(matchData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/maxPlayerOfTheMatchPerSeason.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

csv()
  .fromFile(csvMatchFilePath)
  .then((matchData) => {
    csv()
      .fromFile(csvDeliveriesFilePath)
      .then((deliveriesData) => {
        let result = strikeRate(deliveriesData, matchData);
        fs.writeFile(
          "/home/phanipolina/Desktop/ipl/src/public/output/strikeRate.json",
          JSON.stringify(result),
          function (err) {
            if (err) throw err;
            console.log("Results pushed sucessfully.");
          }
        );
      });
  });

csv()
  .fromFile(csvDeliveriesFilePath)
  .then((deliveriesData) => {
    let result = playerDismissedByPlayer(deliveriesData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/playerDismissedByPlayer.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

csv()
  .fromFile(csvDeliveriesFilePath)
  .then((deliveriesData) => {
    let result = economicBowlerSuperOver(deliveriesData);
    fs.writeFile(
      "/home/phanipolina/Desktop/ipl/src/public/output/economicBowlerSuperOver.json",
      JSON.stringify(result),
      function (err) {
        if (err) throw err;
        console.log("Results pushed sucessfully.");
      }
    );
  });

function economicBowlers(matchData, deliveriesData) {
  let Bowlers = {};
  for (let index in matchData) {
    let year = matchData[index].season;
    if (year === "2015") {
      let matchMatchId = matchData[index].id;
      let ballsBowled = 0;
      let runsConceded = 0;
      let NameOfBowler = "";
      for (let delIndex in deliveriesData) {
        let deleveriesMatchId = deliveriesData[delIndex].match_id;
        let bowlerName = deliveriesData[delIndex].bowler;
        let runsGiven = deliveriesData[delIndex].total_runs;
        if (matchMatchId === deleveriesMatchId) {
          if (bowlerName in Bowlers) {
            Bowlers[bowlerName].ballsBowled += 1;
            Bowlers[bowlerName].runsConceded += parseInt(runsGiven);
          } else {
            Bowlers[bowlerName] = {};
            Bowlers[bowlerName].ballsBowled = 1;
            Bowlers[bowlerName].runsConceded = parseInt(runsGiven);
          }
        }
      }
    }
  }
  let bowlerEconomies = {};
  for (let bowler in Bowlers) {
    let thisBowler = Bowlers[bowler];
    let bowlerEconomy = (
      (thisBowler.ballsBowled / thisBowler.runsConceded) *
      6
    ).toFixed(2);
    bowlerEconomies[bowler] = bowlerEconomy;
  }
  let BowlerEconomiesArray = Object.entries(bowlerEconomies);
  BowlerEconomiesArray.sort((a, b) => a[1] - b[1]);

  let topEconomicalBowlers = BowlerEconomiesArray.slice(0, 10);

  let topEconomicalBowlersObj = [];
  topEconomicalBowlers.forEach((value) => {
    topEconomicalBowlersObj.push({
      BowlerName: value[0],
      EconomyRate: value[1],
    });
  });
  return topEconomicalBowlersObj;
}

module.exports = economicBowlers;

function strikeRate(deliveriesData, matchData) {
  let batsmanData = {};
  for (let delivery of deliveriesData) {
    let batsManName = delivery.batsman;
    let deliveryMatchId = delivery.match_id;
    let runsGot = parseInt(delivery.batsman_runs);

    for (let match of matchData) {
      let matchMatchId = match.id;
      let season = match.season;

      if (matchMatchId === deliveryMatchId) {
        if (!(batsManName in batsmanData)) {
          batsmanData[batsManName] = {};
          batsmanData[batsManName][season] = {
            ballsFaced: 0,
            runsScored: 0,
          };
          batsmanData[batsManName][season]["ballsFaced"] += 1;
          batsmanData[batsManName][season]["runsScored"] += runsGot;
        } else {
          if (season in batsmanData[batsManName]) {
            batsmanData[batsManName][season]["ballsFaced"] += 1;
            batsmanData[batsManName][season]["runsScored"] += runsGot;
          } else {
            batsmanData[batsManName][season] = {
              ballsFaced: 0,
              runsScored: 0,
            };
            batsmanData[batsManName][season]["ballsFaced"] += 1;
            batsmanData[batsManName][season]["runsScored"] += runsGot;
          }
        }
      }
    }
  }

  let bowlerStrikerRate = {};
  let result = batsmanData;
  for (let index in result) {
    let everyStrikeRate = {};

    for (let yearindex in result[index]) {
      let playerstat = result[index][yearindex];
      let playerStrikeRate = (
        (playerstat.runsScored / playerstat.ballsFaced) *
        100
      ).toFixed(2);
      everyStrikeRate[yearindex] = playerStrikeRate;
    }
    bowlerStrikerRate[index] = everyStrikeRate;
  }

  return bowlerStrikerRate;
}

module.exports = strikeRate;

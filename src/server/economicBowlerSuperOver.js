function economicBowlerSuperOver(deliveriesData) {
  let Bowlers = {};
  for (let delivery of deliveriesData) {
    let bowlerName = delivery.bowler;
    let runsGiven = delivery.total_runs;
    let isSuperOver = (delivery.is_super_over === "1");
    if (isSuperOver) {
      if (bowlerName in Bowlers) {
        Bowlers[bowlerName].ballsBowled += 1;
        Bowlers[bowlerName].runsConceded += parseInt(runsGiven);
      } else {
        Bowlers[bowlerName] = {};
        Bowlers[bowlerName].ballsBowled = 1;
        Bowlers[bowlerName].runsConceded = parseInt(runsGiven);
      }
    }
  }

  let bowlerEconomies = {};
  for (let bowler in Bowlers) {
    let thisBowler = Bowlers[bowler];
    let bowlerEconomy = (
      (thisBowler.ballsBowled / thisBowler.runsConceded) *
      6
    ).toFixed(2);
    bowlerEconomies[bowler] = bowlerEconomy;
  }
  let BowlerEconomiesArray = Object.entries(bowlerEconomies);
  BowlerEconomiesArray.sort((a, b) => a[1] - b[1]);

  let topEconomicalBowler = BowlerEconomiesArray[0];

  let topEconomicalBowlerObj = {
    "Bowler Name":topEconomicalBowler[0],
    "Economy Rate":parseFloat(topEconomicalBowler[1])
  };
  return topEconomicalBowlerObj;
}

module.exports = economicBowlerSuperOver;

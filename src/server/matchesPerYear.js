function matchesPerYear(matchData) {
  let matchesPerYerObj = {};
  for (let index in matchData) {
    let year = matchData[index].season;
    if (year in matchesPerYerObj) {
      matchesPerYerObj[year] += 1;
    } else {
      matchesPerYerObj[year] = 1;
    }
  }
  return matchesPerYerObj;
}

module.exports = matchesPerYear;

function playerDismissedByPlayer(deliveriesData) {
  let dismissalObj = {};
  for (let delivery of deliveriesData) {
    let batsman = delivery.player_dismissed;
    let dismisser = delivery.bowler;

    if (batsman !== "") {
      if (`${batsman} dismissed by ${dismisser}` in dismissalObj) {
        dismissalObj[`${batsman} dismissed by ${dismisser}`] += 1;
      } else {
        dismissalObj[`${batsman} dismissed by ${dismisser}`] = 1;
      }
    }
  }

  let dimisaalValues = Object.values(dismissalObj);
  let maxDismissals = {};
  let maxDismissalValue = Math.max(...dimisaalValues);
  for (let dismissal in dismissalObj) {
    if (dismissalObj[dismissal] === maxDismissalValue) {
      maxDismissals[dismissal] = maxDismissalValue;
    }
  }

  return maxDismissals;
}

module.exports = playerDismissedByPlayer;

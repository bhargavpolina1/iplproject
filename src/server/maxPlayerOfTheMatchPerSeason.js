function maxPlayerOfTheMatchPerSeason(matchData) {
  let seasons = [];
  for (let i = 2008; i < 2018; i++) {
    seasons.push(i);
  }

  let playerOfTheMatch = {};
  for (let i in seasons) {
    let year = seasons[i];
    for (let match in matchData) {
      let season = matchData[match].season;
      let thisMatchPlayer = matchData[match].player_of_match;
      if (year == season) {
        if (year in playerOfTheMatch) {
          if (thisMatchPlayer in playerOfTheMatch[year]) {
            playerOfTheMatch[year][thisMatchPlayer] += 1;
          } else {
            playerOfTheMatch[year][thisMatchPlayer] = 1;
          }
        } else {
          playerOfTheMatch[year] = {};

          playerOfTheMatch[year][thisMatchPlayer] = 1;
        }
      }
    }
  }

  let bestPlayerOftheMatches = {};
  for (let everyYear in playerOfTheMatch) {
    yearsObj = playerOfTheMatch[everyYear];
    const valuesArray = Object.values(yearsObj);
    const maxPlayerOftheMatchValue = Math.max(...valuesArray);

    for (let everyPlayer in yearsObj) {
      if (yearsObj[everyPlayer] === maxPlayerOftheMatchValue) {
        if (everyYear in bestPlayerOftheMatches) {
          bestPlayerOftheMatches[everyYear][everyPlayer] =
            maxPlayerOftheMatchValue;
        } else {
          bestPlayerOftheMatches[everyYear] = {};
          bestPlayerOftheMatches[everyYear][everyPlayer] =
            maxPlayerOftheMatchValue;
        }
      }
    }
  }

  return bestPlayerOftheMatches;
}

module.exports = maxPlayerOfTheMatchPerSeason;

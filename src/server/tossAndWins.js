function tossAndWins(matchData) {
  let tossAndWins = {};
  for (let index in matchData) {
    let match = matchData[index];
    let tossWinner = match.toss_winner;
    let matchWinner = match.winner;

    if (tossWinner === matchWinner) {
      if (tossWinner in tossAndWins) {
        tossAndWins[tossWinner] += 1;
      } else {
        tossAndWins[tossWinner] = 1;
      }
    }
  }

  return tossAndWins;
}

module.exports = tossAndWins;

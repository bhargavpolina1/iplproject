function extraRunsPerTeam(deliveriesData, matchData) {
  let extraRunsPerTeamObj = {};
  for (let index in matchData) {
    let year = matchData[index].season;
    if (year === "2016") {
      let team = matchData[index].team1;
      let matchMatchId = matchData[index].id;
      for (let delIndex in deliveriesData) {
        let deleveriesMatchId = deliveriesData[delIndex].match_id;
        let extraRunsOfBall = deliveriesData[delIndex].extra_runs;
        let inningsId = deliveriesData[delIndex].inning;
        if (matchMatchId === deleveriesMatchId && inningsId === "1") {
          if (team in extraRunsPerTeamObj) {
            extraRunsPerTeamObj[team] += parseInt(extraRunsOfBall);
          } else {
            extraRunsPerTeamObj[team] = parseInt(extraRunsOfBall);
          }
        }
      }
    }
  }

  return extraRunsPerTeamObj;
}

module.exports = extraRunsPerTeam;
